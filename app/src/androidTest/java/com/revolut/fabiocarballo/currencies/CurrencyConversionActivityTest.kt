package com.revolut.fabiocarballo.currencies

import android.support.test.InstrumentationRegistry
import android.support.test.rule.ActivityTestRule
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.revolut.fabiocarballo.api.RevolutApi
import com.revolut.fabiocarballo.api.RevolutCurrenciesBundle
import com.revolut.fabiocarballo.api.RevolutCurrencyRate
import com.revolut.fabiocarballo.api.RevolutCurrencyRates
import com.revolut.fabiocarballo.internals.BaseAppController
import com.revolut.fabiocarballo.internals.di.AppComponent
import com.revolut.fabiocarballo.internals.di.NetworkModule
import it.cosenonjaviste.daggermock.DaggerMock
import org.junit.Rule
import org.junit.Test
import rx.Observable
import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.*

class CurrencyConversionActivityTest {

    @get:Rule
    val daggerRule = DaggerMock.rule<AppComponent>(NetworkModule()) {
        set {
            val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as BaseAppController
            app.appComponent = it
        }
    }

    @get:Rule
    var activityRule = ActivityTestRule(CurrencyConversionActivity::class.java, false, false)

    val api: RevolutApi = mock()

    val formatter: DecimalFormat = DecimalFormat("#0.00")

    @Test
    fun data_is_displayed() {
        val bulgarian = Currency.getInstance("BGN")
        val brazilianReal = Currency.getInstance("BRL")
        val australianDollar = Currency.getInstance("AUD")
        val euro = Currency.getInstance("EUR")

        val serverRates = listOf(
                RevolutCurrencyRate(bulgarian, BigDecimal(1.95)),
                RevolutCurrencyRate(brazilianReal, BigDecimal(3.83)),
                RevolutCurrencyRate(australianDollar, BigDecimal(1.55)))

        val serverResponse = RevolutCurrenciesBundle(
                base = euro,
                rates = RevolutCurrencyRates(serverRates))



        whenever(api.getCurrencies()).thenReturn(Observable.just(serverResponse))

        activityRule.launchActivity(null)

        CurrencyConversionTestingRobot.assert {
            hasSize(4)

            hasItem(0, "Euro", formatter.format(BigDecimal("1")))
            hasItem(1, "Bulgarian Lev", formatter.format(BigDecimal("1.95")))
            hasItem(2, "Brazilian Real", formatter.format(BigDecimal("3.83")))
            hasItem(3, "Australian Dollar", formatter.format(BigDecimal("1.55")))
        }
    }
}