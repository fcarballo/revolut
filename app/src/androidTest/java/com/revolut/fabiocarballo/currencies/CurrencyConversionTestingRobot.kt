package com.revolut.fabiocarballo.currencies


import android.support.test.espresso.Espresso
import android.support.test.espresso.assertion.ViewAssertions.*
import android.support.test.espresso.matcher.ViewMatchers.*
import com.revolut.fabiocarballo.R
import com.revolut.fabiocarballo.RecyclerViewItemCountAssertion
import com.revolut.fabiocarballo.RecyclerViewMatcher

class CurrencyConversionTestingRobot {

    companion object {
        fun assert(func: CurrencyConversionTestingRobot.() -> Unit) = CurrencyConversionTestingRobot().apply(func)
    }

    fun hasSize(size: Int) {
        Espresso.onView(withId(R.id.recyclerView))
                .check(RecyclerViewItemCountAssertion(size))
    }

    fun hasItem(position: Int, currencyCode: String, value: String) {
        Espresso.onView(RecyclerViewMatcher(R.id.recyclerView).atPosition(position))
                .check(matches(hasDescendant(withText(currencyCode))))
                .check(matches(hasDescendant(withText(value))))
    }
}