package com.revolut.fabiocarballo

import android.app.Activity
import android.view.inputmethod.InputMethodManager
import org.jetbrains.anko.inputMethodManager


class Keyboard(private val activity: Activity) {

    fun open() {
        activity.inputMethodManager
                .toggleSoftInputFromWindow(
                        activity.window.decorView.windowToken,
                        InputMethodManager.SHOW_FORCED,
                        InputMethodManager.HIDE_NOT_ALWAYS)
    }
}