package com.revolut.fabiocarballo.currencies

import dagger.Subcomponent

@Subcomponent(modules = arrayOf(CurrenciesModule::class))
interface CurrenciesComponent {

    @Subcomponent.Builder
    interface Builder {
        fun plus(module: CurrenciesModule): Builder
        fun build(): CurrenciesComponent
    }

    fun inject(activity: CurrencyConversionActivity)

    fun inject(adapter: CurrencyConversionAdapter)
}