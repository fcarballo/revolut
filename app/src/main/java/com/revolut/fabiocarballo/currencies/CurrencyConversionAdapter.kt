package com.revolut.fabiocarballo.currencies

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.revolut.fabiocarballo.Keyboard
import com.revolut.fabiocarballo.R
import com.revolut.fabiocarballo.domain.Conversion
import com.revolut.fabiocarballo.domain.InvalidConversion
import com.revolut.fabiocarballo.domain.ValidConversion
import kotlinx.android.synthetic.main.elem_currency_rate.view.*
import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.*
import javax.inject.Inject

class CurrencyConversionAdapter(
        private val context: Context,
        private val currencyValueUpdateListener: CurrencyValueUpdateListener) : RecyclerView.Adapter<CurrencyConversionAdapter.ViewHolder>() {

    @Inject
    lateinit var keyboard: Keyboard

    var conversionsList: List<Conversion> = mutableListOf()
        set(value) {
            if (field.isEmpty()) {
                field = value
                notifyDataSetChanged()
            } else {
                field = value
                notifyItemRangeChanged(1, currencies.size - 1)
            }
        }

    private val currencies: MutableList<Currency> by lazy {
        conversionsList.map { it.currency }.toMutableList()
    }

    private val inflater by lazy { LayoutInflater.from(context) }
    private val format by lazy { DecimalFormat("#0.00") }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.elem_currency_rate, parent, false)

        return ViewHolder(view, currencies, currencyValueUpdateListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currency = currencies[position]
        val conversion = conversionsList.first { it.currency == currency }

        holder.itemView.let {
            it.currency.text = currency.currencyCode
            it.currencyName.text = currency.displayName

            when (conversion) {
                is InvalidConversion -> it.conversion.setText("")
                is ValidConversion -> it.conversion.setText(format.format(conversion.value))
            }

            if (position == 0) {
                it.conversion.requestFocus()
            }

            it.setOnClickListener {
                keyboard.open()

                currencies.removeAt(position)
                currencies.add(0, currency)

                notifyItemMoved(position, 0)
                notifyItemRangeChanged(0, currencies.size)
            }
        }
    }

    override fun getItemCount() = conversionsList.size

    class ViewHolder(view: View,
                     val currencies: List<Currency>,
                     val listener: CurrencyValueUpdateListener) : RecyclerView.ViewHolder(view) {

        init {
            view.conversion.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    // empty
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    // empty
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (adapterPosition == 0) {
                        val currencyValue = try {
                            BigDecimal(s.toString())
                        } catch (exception: NumberFormatException) {
                            null
                        }

                        val currency = currencies.first()

                        listener.onCurrencyValueUpdate(currency, currencyValue)
                    }
                }
            })
        }
    }


    interface CurrencyValueUpdateListener {
        fun onCurrencyValueUpdate(currency: Currency, value: BigDecimal?)
    }
}