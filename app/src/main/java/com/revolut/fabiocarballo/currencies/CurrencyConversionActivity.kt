package com.revolut.fabiocarballo.currencies

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.revolut.fabiocarballo.R
import com.revolut.fabiocarballo.domain.Conversion
import com.revolut.fabiocarballo.extensions.toGone
import com.revolut.fabiocarballo.extensions.toVisible
import com.revolut.fabiocarballo.internals.BaseActivity
import kotlinx.android.synthetic.main.activity_currencies.*
import kotlinx.android.synthetic.main.view_error.*
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

class CurrencyConversionActivity : BaseActivity(), CurrencyConversionPresenter.View {

    @Inject
    lateinit var presenter: CurrencyConversionPresenter

    private val currencyValueUpdateListener = object : CurrencyConversionAdapter.CurrencyValueUpdateListener {
        override fun onCurrencyValueUpdate(currency: Currency, value: BigDecimal?) {
            presenter.onCurrencyValueChange(currency, value)
        }
    }

    private val currencyRateAdapter by lazy { CurrencyConversionAdapter(this, currencyValueUpdateListener) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_currencies)

        val component = activityComponent.currenciesComponentBuilder()
                .plus(CurrenciesModule(this))
                .build()

        component.let {
            it.inject(this)
            it.inject(currencyRateAdapter)
        }

        with(recyclerView) {
            adapter = currencyRateAdapter
            layoutManager = LinearLayoutManager(this@CurrencyConversionActivity, LinearLayoutManager.VERTICAL, false)
        }

        currencyRateAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {

            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                if (toPosition == 0) {
                    recyclerView.layoutManager.scrollToPosition(0)
                }
            }
        })

        refreshLayout.setOnRefreshListener { presenter.start() }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onStop() {
        super.onStop()
        presenter.stop()
    }

    override fun setConversions(conversions: List<Conversion>) {
        recyclerView.toVisible()
        errorView.toGone()
        hideLoaders()

        currencyRateAdapter.conversionsList = conversions.toMutableList()
    }

    private fun hideLoaders() {
        refreshLayout.isRefreshing = false
        loadingBar.toGone()
    }

    override fun showNoConnectionAlert() {
        errorView.toVisible()
        recyclerView.toGone()
        hideLoaders()

        errorMsg.text = getString(R.string.error_no_connection)
    }

}