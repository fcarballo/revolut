package com.revolut.fabiocarballo.currencies

import com.revolut.fabiocarballo.domain.Conversion
import com.revolut.fabiocarballo.domain.ConversionCalculator
import com.revolut.fabiocarballo.internals.Logger
import com.revolut.fabiocarballo.internals.exceptions.NoConnectionException
import rx.Observable
import rx.Scheduler
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit

class CurrencyConversionPresenter(
        private val view: View,
        private val getCurrencyRates: GetCurrencyRates,
        private val conversionCalculator: ConversionCalculator,
        private val logger: Logger = Logger(),
        private val scheduler: Scheduler? = Schedulers.computation()) {

    private val subscriptions = CompositeSubscription()

    fun start() {
        // since this can be called on refresh, it is needed to clear any previous subscriptions
        subscriptions.clear()

        subscriptions.add(
                Observable.just(0L)
                        .concatWith(Observable.interval(1, TimeUnit.SECONDS, scheduler)
                                .doOnNext { System.out.print("HERE") })
                        .flatMapSingle { getCurrencyRates.build() }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    logger.debug("updated currency rates!")

                                    conversionCalculator.updateRates(it)
                                },
                                {
                                    logger.error(it)
                                    
                                    when (it) {
                                        is NoConnectionException -> view.showNoConnectionAlert()
                                        else -> throw it
                                    }
                                })
        )

        subscriptions.add(
                conversionCalculator.conversions()
                        .distinctUntilChanged()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { view.setConversions(it) },
                                { logger.error(it) })
        )
    }

    fun stop() {
        subscriptions.clear()
    }


    fun onCurrencyValueChange(currency: Currency, value: BigDecimal?) {
        conversionCalculator.updateComparisonConversion(currency, value)
    }

    interface View {
        fun setConversions(conversions: List<Conversion>)
        fun showNoConnectionAlert()
    }

}