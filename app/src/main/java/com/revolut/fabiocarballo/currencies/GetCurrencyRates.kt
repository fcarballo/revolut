package com.revolut.fabiocarballo.currencies

import com.revolut.fabiocarballo.api.RevolutApi
import com.revolut.fabiocarballo.domain.CurrencyRate
import rx.Single
import java.math.BigDecimal

class GetCurrencyRates(
        private val api: RevolutApi) {

    fun build(): Single<List<CurrencyRate>> {
        return api.getCurrencies()
                .map { bundle ->
                    val baseCurrency = CurrencyRate(BigDecimal(1), bundle.base)
                    val currenciesRates = bundle.rates.currenciesRates.map { CurrencyRate(it.rate, it.currency) }

                    listOf(baseCurrency) + currenciesRates
                }
                .first()
                .toSingle()
    }
}