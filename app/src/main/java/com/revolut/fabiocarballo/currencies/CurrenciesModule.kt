package com.revolut.fabiocarballo.currencies

import com.revolut.fabiocarballo.api.RevolutApi
import com.revolut.fabiocarballo.domain.ConversionCalculator
import dagger.Module
import dagger.Provides

@Module
class CurrenciesModule(private val view: CurrencyConversionPresenter.View) {

    @Provides
    fun provideCurrenciesPresenter(getCurrenciesUseCase: GetCurrencyRates,
                                   conversionCalculator: ConversionCalculator): CurrencyConversionPresenter {
        return CurrencyConversionPresenter(view, getCurrenciesUseCase, conversionCalculator)
    }

    @Provides
    fun provideGetCurrenciesUseCase(api: RevolutApi): GetCurrencyRates {
        return GetCurrencyRates(api)
    }

    @Provides
    fun provideConversionsCalculator() = ConversionCalculator()
}