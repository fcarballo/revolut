package com.revolut.fabiocarballo.internals.di

import com.revolut.fabiocarballo.currencies.CurrenciesComponent
import dagger.Subcomponent

@Subcomponent(modules = arrayOf(ActivityModule::class))
@ActivityScope
interface ActivityComponent {

    @Subcomponent.Builder
    interface Builder {
        fun plus(module: ActivityModule): Builder
        fun build(): ActivityComponent
    }

    fun currenciesComponentBuilder(): CurrenciesComponent.Builder
}