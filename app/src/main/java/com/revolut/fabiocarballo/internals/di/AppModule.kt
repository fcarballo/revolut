package com.revolut.fabiocarballo.internals.di

import android.app.Application
import com.revolut.fabiocarballo.internals.Logger
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(
        private val app: Application) {

    @Provides
    @Singleton
    fun provideLogger(): Logger {
        return Logger()
    }
}