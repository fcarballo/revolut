package com.revolut.fabiocarballo.internals.di


interface BaseAppComponent {

    fun activityComponentBuilder(): ActivityComponent.Builder
}
