package com.revolut.fabiocarballo.internals.di

import android.app.Activity
import com.revolut.fabiocarballo.Keyboard
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: Activity) {

    @Provides
    @ActivityScope
    fun provideActivity(): Activity {
        return activity
    }

    @Provides
    fun provideKeyboard(): Keyboard {
        return Keyboard(activity)
    }
}