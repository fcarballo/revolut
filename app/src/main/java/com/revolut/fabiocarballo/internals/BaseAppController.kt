package com.revolut.fabiocarballo.internals

import android.app.Application
import com.revolut.fabiocarballo.internals.di.AppModule
import com.revolut.fabiocarballo.internals.di.BaseAppComponent
import com.revolut.fabiocarballo.internals.di.DaggerAppComponent

abstract class BaseAppController : Application() {

    lateinit var appComponent: BaseAppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this@BaseAppController))
                .build()
    }
}