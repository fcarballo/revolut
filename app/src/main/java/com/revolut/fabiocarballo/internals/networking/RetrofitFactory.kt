package com.revolut.fabiocarballo.internals.networking

import com.google.gson.Gson
import com.revolut.fabiocarballo.internals.RxErrorHandlingCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class RetrofitFactory {

    fun build(client: OkHttpClient = OkHttpClient(),
              gson: Gson = Gson()): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
    }

    companion object {
        private const val BASE_URL = "https://revolut.duckdns.org/"
    }
}