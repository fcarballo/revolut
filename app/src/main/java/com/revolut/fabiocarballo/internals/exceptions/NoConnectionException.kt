package com.revolut.fabiocarballo.internals.exceptions

class NoConnectionException: RuntimeException()