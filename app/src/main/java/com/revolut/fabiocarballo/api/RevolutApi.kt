package com.revolut.fabiocarballo.api

import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface RevolutApi {

    @GET("latest")
    fun getCurrencies(@Query("base") base: String = "EUR"): Observable<RevolutCurrenciesBundle>

}
