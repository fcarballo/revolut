package com.revolut.fabiocarballo.api

import java.math.BigDecimal
import java.util.*

data class RevolutCurrenciesBundle(
        val base: Currency,
        val rates: RevolutCurrencyRates)

data class RevolutCurrencyRates(val currenciesRates: List<RevolutCurrencyRate>)

data class RevolutCurrencyRate(
        val currency: Currency,
        val rate: BigDecimal)

