package com.revolut.fabiocarballo.api

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.math.BigDecimal
import java.util.*

class RevolutCurrencyRatesDeserializer : JsonDeserializer<RevolutCurrencyRates> {

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext): RevolutCurrencyRates {
        val currenciesRates = json.asJsonObject.entrySet().map {
            val currency = Currency.getInstance(it.key)
            val rate = it.value.asString

            RevolutCurrencyRate(currency, BigDecimal(rate))
        }

        return RevolutCurrencyRates(currenciesRates)
    }
}