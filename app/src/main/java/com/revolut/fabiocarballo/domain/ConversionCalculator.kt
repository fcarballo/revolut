package com.revolut.fabiocarballo.domain

import rx.Observable
import rx.subjects.PublishSubject
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

class ConversionCalculator {

    private lateinit var rates: Map<Currency, BigDecimal>
    private lateinit var baseCurrencyRate: CurrencyRate

    private var comparisonConversion: Conversion? = null

    private val conversionsPubSub: PublishSubject<List<Conversion>> = PublishSubject.create()

    fun updateRates(rates: List<CurrencyRate>) {
        this.rates = rates.associateBy({ it.currency }, { it.rate })
        this.baseCurrencyRate = rates.first()

        // in the first time we update the rates we set the basis of the conversion as the
        // base currency rate
        if (comparisonConversion == null) {
            comparisonConversion = ValidConversion(baseCurrencyRate.currency, baseCurrencyRate.rate)
        }

        broadcastConversion()
    }

    fun updateComparisonConversion(currency: Currency, value: BigDecimal?) {
        comparisonConversion = value?.let {
            calculateConversion(it, currency, baseCurrencyRate.currency)
        } ?: InvalidConversion(currency)

        broadcastConversion()
    }

    fun conversions(): Observable<List<Conversion>> {
        return conversionsPubSub
    }

    private fun broadcastConversion() {
        val conversions = rates.entries.map {
            val (currency, rate) = it

            comparisonConversion?.let { conversion ->
                when (conversion) {
                    is ValidConversion -> ValidConversion(currency, conversion.value * rate)
                    else -> InvalidConversion(currency)
                }
            } ?: InvalidConversion(currency)
        }

        conversionsPubSub.onNext(conversions)
    }

    private fun calculateConversion(value: BigDecimal, from: Currency, to: Currency): Conversion {
        return ValidConversion(
                to,
                value.multiply(rates[to]).divide(rates[from], 10, RoundingMode.HALF_EVEN))
    }
}