package com.revolut.fabiocarballo.domain

import java.math.BigDecimal
import java.util.Currency

data class CurrencyRate(
        val rate: BigDecimal,
        val currency: Currency)
