package com.revolut.fabiocarballo.domain

import java.math.BigDecimal
import java.util.*

sealed class Conversion(val currency: Currency)

class ValidConversion(
        currency: Currency,
        val value: BigDecimal) : Conversion(currency)

class InvalidConversion(
        currency: Currency) : Conversion(currency)