package com.revolut.fabiocarballo.currencies

import com.nhaarman.mockito_kotlin.*
import com.revolut.fabiocarballo.domain.Conversion
import com.revolut.fabiocarballo.domain.ConversionCalculator
import com.revolut.fabiocarballo.domain.CurrencyRate
import com.revolut.fabiocarballo.domain.ValidConversion
import com.revolut.fabiocarballo.internals.Logger
import com.revolut.fabiocarballo.rxGroup
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import rx.Observable
import rx.Single
import rx.schedulers.TestScheduler
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit

class CurrencyConversionPresenterSpec : Spek({

    rxGroup("CurrencyConversionPresenter") {

        val getCurrenciesUseCase: GetCurrencyRates = mock()
        val conversionCalculator: ConversionCalculator = mock()
        val view: CurrencyConversionPresenter.View = mock()
        val logger: Logger = mock()

        val testScheduler: TestScheduler = TestScheduler()

        val tested = CurrencyConversionPresenter(
                view,
                getCurrenciesUseCase,
                conversionCalculator,
                logger,
                testScheduler)


        beforeEachTest {
            reset(view)
            reset(getCurrenciesUseCase)
            reset(conversionCalculator)
        }


        describe("#onCurrencyValueChange") {
            val euro = Currency.getInstance("EUR")
            val value = BigDecimal(10.0)

            beforeEachTest {
                tested.onCurrencyValueChange(euro, value)
            }

            it("should update the conversion calculator with the new base conversion value") {
                verify(conversionCalculator).updateComparisonConversion(euro, value)
            }
        }

        describe("currency rates update") {
            val staticRates = listOf(
                    CurrencyRate(BigDecimal(1), Currency.getInstance("EUR")),
                    CurrencyRate(BigDecimal(1.5), Currency.getInstance("BRL")),
                    CurrencyRate(BigDecimal(1.4), Currency.getInstance("AUD")))

            beforeEachTest {
                whenever(getCurrenciesUseCase.build()).thenReturn(Single.just(staticRates))
                whenever(conversionCalculator.conversions()).thenReturn(Observable.empty<List<Conversion>>())

                tested.start()
            }

            afterEachTest {
                tested.stop()
            }

            it("should request the currency rates") {
                verify(getCurrenciesUseCase).build()
            }

            it("should update conversions calculator with the new rates") {
                verify(conversionCalculator).updateRates(staticRates)
            }

            given("that half a second has passed") {
                beforeEachTest {
                    testScheduler.advanceTimeBy(500, TimeUnit.MILLISECONDS)
                    testScheduler.triggerActions()
                }

                it("should not have request the currency rates for a second time") {
                    verify(getCurrenciesUseCase, times(1)).build()
                }

                given("that a second has passed") {
                    beforeEachTest {
                        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS)
                        testScheduler.triggerActions()
                    }

                    it("should have request the currency rates for a second time") {
                        verify(getCurrenciesUseCase, times(2)).build()
                    }
                }
            }
        }

        describe("conversions update to the view") {
            val conversions = listOf<Conversion>(
                    ValidConversion(Currency.getInstance("EUR"), BigDecimal(1)),
                    ValidConversion(Currency.getInstance("BRL"), BigDecimal(1.5)),
                    ValidConversion(Currency.getInstance("AUD"), BigDecimal(1.4)))

            beforeEachTest {
                whenever(getCurrenciesUseCase.build()).thenReturn(Single.just(listOf()))
                whenever(conversionCalculator.conversions()).thenReturn(Observable.just(conversions))

                tested.start()
            }

            afterEachTest {
                tested.stop()
            }

            it("should listen to conversion updates") {
                verify(conversionCalculator).conversions()
            }

            it("should display the conversion update on the view") {
                verify(view).setConversions(conversions)
            }
        }
    }
})