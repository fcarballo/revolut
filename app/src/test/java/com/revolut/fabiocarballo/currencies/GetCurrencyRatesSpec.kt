package com.revolut.fabiocarballo.currencies

import com.nhaarman.mockito_kotlin.*
import com.revolut.fabiocarballo.api.RevolutApi
import com.revolut.fabiocarballo.api.RevolutCurrenciesBundle
import com.revolut.fabiocarballo.api.RevolutCurrencyRate
import com.revolut.fabiocarballo.api.RevolutCurrencyRates
import com.revolut.fabiocarballo.domain.CurrencyRate
import com.revolut.fabiocarballo.internals.exceptions.NoConnectionException
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.context
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import rx.observers.TestSubscriber
import rx.subjects.PublishSubject
import java.math.BigDecimal
import java.util.*

class GetCurrencyRatesSpec : Spek({

    given("GetCurrencyRates") {
        val api: RevolutApi = mock()
        val tested = GetCurrencyRates(api)

        var apiCurrencyRatesPubSub: PublishSubject<RevolutCurrenciesBundle> = PublishSubject.create()

        var testSubscriber: TestSubscriber<List<CurrencyRate>> = TestSubscriber()

        beforeEachTest {
            reset(api)

            apiCurrencyRatesPubSub = PublishSubject.create()
            whenever(api.getCurrencies(any())).thenReturn(apiCurrencyRatesPubSub)

            testSubscriber = TestSubscriber()
        }

        describe("#build") {

            beforeEachTest {
                tested.build().subscribe(testSubscriber)
            }

            it("should request the currency rates to the revolut api") {
                verify(api).getCurrencies(any())
            }

            context("on successful request") {
                val bulgarian = Currency.getInstance("BGN")
                val brazilianReal = Currency.getInstance("BRL")
                val australianDollar = Currency.getInstance("AUD")
                val euro = Currency.getInstance("EUR")

                val serverRates = listOf(
                        RevolutCurrencyRate(bulgarian, BigDecimal(1.95)),
                        RevolutCurrencyRate(brazilianReal, BigDecimal(3.83)),
                        RevolutCurrencyRate(australianDollar, BigDecimal(1.55)))

                val serverResponse = RevolutCurrenciesBundle(
                        base = euro,
                        rates = RevolutCurrencyRates(serverRates))

                beforeEachTest {
                    apiCurrencyRatesPubSub.onNext(serverResponse)
                    apiCurrencyRatesPubSub.onCompleted()
                }

                it("should return all the currency rates") {
                    val expected = listOf(
                            CurrencyRate(BigDecimal(1), euro),
                            CurrencyRate(BigDecimal(1.95), bulgarian),
                            CurrencyRate(BigDecimal(3.83), brazilianReal),
                            CurrencyRate(BigDecimal(1.55), australianDollar))

                    testSubscriber.assertValue(expected)
                    testSubscriber.assertValueCount(1)
                }
            }

            context("on a failed request") {
                val exception = NoConnectionException()

                beforeEachTest {
                    apiCurrencyRatesPubSub.onError(exception)
                }

                it("should return the thrown exception") {
                    testSubscriber.assertError(exception)
                }
            }
        }
    }
})