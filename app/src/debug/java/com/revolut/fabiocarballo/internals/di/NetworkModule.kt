package com.revolut.fabiocarballo.internals.di

import com.revolut.fabiocarballo.api.RevolutApi
import com.revolut.fabiocarballo.internals.networking.RetrofitFactory
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.revolut.fabiocarballo.api.RevolutCurrencyRates
import com.revolut.fabiocarballo.api.RevolutCurrencyRatesDeserializer
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideApi(retrofitFactory: RetrofitFactory,
                   httpClient: OkHttpClient,
                   gson: Gson): RevolutApi {
        return retrofitFactory.build(httpClient, gson).create(RevolutApi::class.java)
    }

    @Provides
    fun provideClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .addNetworkInterceptor(StethoInterceptor())
                .build()
    }

    @Provides
    fun provideRetrofitFactory(): RetrofitFactory {
        return RetrofitFactory()
    }

    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
                .registerTypeAdapter(RevolutCurrencyRates::class.java, RevolutCurrencyRatesDeserializer())
                .create()
    }
}